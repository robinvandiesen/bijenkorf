<?php include 'inc/head.php'; ?>

<body>
<a class="accessibility skip-to-main" href="#main">Skip to main content</a>

<!-- Page -->
<div class="page">

    <?php include 'inc/menu.php'; ?>
    
    <!-- Section -->
    <section class="section" id="main" tabindex="-1">
        <div class="section-inner">

            <h1>404 error - Page not found</h1> 
            
            <p>This content is not (or no longer) available.</p>
            <p>Please go back to <a href="/">home</a>, use the search or report this problem via <a href="mailto:info@website.com">info@website.com</a>.</p>

        </div>
    </section>
    <!-- /Section -->
    
</div>
<!-- /Page -->


<?php include 'inc/script.php'; ?>
</body>
</html>