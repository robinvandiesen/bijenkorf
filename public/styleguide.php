<?php include 'inc/head.php'; ?>

<body>
<style type="text/css">
    .color-item {
        width: 20%;
        float: left;
    }

    .color-preview {
        width: 150px;
        height: 150px;
    }
</style>


<a class="accessibility skip-to-main" href="#main">Skip to main content</a>

<!-- Page -->
<div class="page">

    <?php include 'inc/menu.php'; ?>

    <!-- Section -->
    <section class="section" id="main" tabindex="-1">
        <div class="section__inner content-box">

            <h1>Templates</h1>

            <p><a href="template-overview.php">An overview of the templates</a>.</p>

        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>

    <!-- Section -->
    <section class="section">
        <div class="section__inner content-box">

            <h1>Colors</h1>

            <ul class="list group">
                <li class="color-item">
                    <div class="color-preview" style="background-color: #333;"></div>
                    hex: #333;<br>
                    name: $main-color;
                </li>
                <li class="color-item">
                    <div class="color-preview" style="background-color: #333;"></div>
                    hex: #333;<br>
                    name: $main-color;
                </li>
                <li class="color-item">
                    <div class="color-preview" style="background-color: #333;"></div>
                    hex: #333;<br>
                    name: $main-color;
                </li>
                <li class="color-item">
                    <div class="color-preview" style="background-color: #333;"></div>
                    hex: #333;<br>
                    name: $main-color;
                </li>
                <li class="color-item">
                    <div class="color-preview" style="background-color: #333;"></div>
                    hex: #333;<br>
                    name: $main-color;
                </li>
            </ul>

        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>

    <!-- Section -->
    <section class="section">
        <div class="section__inner content-box">

            <h1>Basic content</h1>
            <br>
            <h1 id="headings">Headings</h1>
            <br>

            <h1>h1. Heading</h1>
            <h2>h2. Heading</h2>
            <h3>h3. Heading</h3>
            <h4>h4. Heading</h4>
            <h5>h5. Heading</h5>
            <h6>h6. Heading</h6>

            <hr>

            <h1 id="paragraph">Paragraph</h1>
            <br>

            <p>Lorem ipsum dolor sit amet, <a href="#" title="test link">test link</a> adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>

            <p>Lorem ipsum dolor sit amet, <em>emphasis</em> consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>

            <hr>

            <h1 id="list_types">List Types</h1>
            <br>

            <h3>Definition List</h3>
            <dl>
                <dt>Definition List Title</dt>
                <dd>This is a definition list division.</dd>
            </dl>
            <br>

            <h3>Ordered List</h3>
            <ol>
                <li>List Item 1</li>
                <li>List Item 2</li>
                <li>List Item 3</li>
            </ol>
            <br>

            <h3>Unordered List</h3>
            <ul>
                <li>List Item 1</li>
                <li>List Item 2</li>
                <li>List Item 3</li>
            </ul>
            <br>

            <h3>%default-ol Ordered List</h3>
            <ol class="default">
                <li>List Item 1</li>
                <li>List Item 2</li>
                <li>List Item 3</li>
            </ol>
            <br>

            <h3>%default-ul Unordered List</h3>
            <ul class="default">
                <li>List Item 1</li>
                <li>List Item 2</li>
                <li>List Item 3</li>
            </ul>

            <hr>

        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>

    <!-- Section -->
    <section class="section">
        <div class="section__inner content-box">

            <h1>Breadcrumbs</h1>

            <ul class="list breadcrumb-list" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link" itemprop="url"><span itemprop="title">Home</span></a></li>
                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link" itemprop="url"><span itemprop="title">Category</span></a></li>
                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link" itemprop="url"><span itemprop="title">Article</span></a></li>
            </ul>

        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>

    <!-- Section -->
    <section class="section">
        <div class="section__inner content-box">

            <h1>Buttons and links</h1>

            <p>Lorem ipsum dolor sit amet, quis nostrud exercitation <a href="#">link in lopende tekst</a> nisi ut.</p>

            <a href="#" class="btn">Button</a>

        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>


    <br>
    <br>

    <!-- Section -->
    <section class="section">
        <div class="section__inner content-box">

            <h1>Form elements</h1>

            <form>
                <div class="fieldset">
                    <label class="label">Textfield</label>
                    <input type="text" class="text">
                </div>

                <div class="fieldset">
                    <input type="radio" name="radio-group" class="radio" checked="checked" id="radio-1">
                    <label class="label" for="radio-1">Radio</label>
                    <input type="radio" name="radio-group" class="radio" id="radio-2">
                    <label class="label" for="radio-2">Radio</label>
                </div>

                <div class="fieldset">
                    <input type="checkbox" class="checkbox" checked="checked" id="checkbox-checked">
                    <label class="label" for="checkbox-checked">Checkbox</label>

                    <input type="checkbox" class="checkbox" id="checkbox-unchecked">
                    <label class="label" for="checkbox-unchecked">Checkbox</label>
                </div>

                <div class="fieldset select-container icon-link">
                    <label class="label">Select</label>
                    <div class="select">
                        <select>
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                        <div class="select-arrow icon-link"></div>
                    </div>
                </div>

                <div class="fieldset">
                    <label class="label">Textarea</label>
                    <textarea class="textarea"></textarea>
                </div>

                <button type="button" class="btn">Button</button>
                <button type="submit" class="btn">Submit</button>
            </form>
        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>

    <!-- Section -->
    <section class="section">
        <div class="section__inner content-box">

            <h1>Form elements - Errors</h1>

            <form>
                <div class="fieldset error">
                    <label class="label">Textfield</label>
                    <input type="text" class="text">
                </div>

                <div class="fieldset error">
                    <input type="radio" name="radio-error-group" class="radio" checked="checked" id="radio-error-1">
                    <label class="label" for="radio-error-1">Radio</label>
                    <input type="radio" name="radio-error-group" class="radio" id="radio-error-2">
                    <label class="label" for="radio-error-2">Radio</label>
                </div>

                <div class="fieldset error">
                    <input type="checkbox" class="checkbox" checked="checked" id="checkbox-error-checked">
                    <label class="label" for="checkbox-error-checked">Checkbox</label>

                    <input type="checkbox" class="checkbox" id="checkbox-error-unchecked">
                    <label class="label" for="checkbox-error-unchecked">Checkbox</label>
                </div>

                <div class="fieldset select-container icon-link error">
                    <label class="label">Select</label>
                    <div class="select">
                        <select>
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                        <div class="select-arrow icon-link"></div>
                    </div>
                </div>

                <div class="fieldset error">
                    <label class="label">Textarea</label>
                    <textarea class="textarea"></textarea>
                </div>

                <button type="button" class="btn">Button</button>
                <button type="submit" class="btn">Submit</button>
            </form>

        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>

    <!-- Section -->
    <section class="section">
        <div class="section__inner content-box">

            <h1 id="tables">Tables</h1>

            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <th>Table Header 1</th>
                        <th>Table Header 2</th>
                        <th>Table Header 3</th>
                    </tr>

                    <tr>
                        <td>Division 1</td>
                        <td>Division 2</td>
                        <td>Division 3</td>
                    </tr>

                    <tr class="even">
                        <td>Division 1</td>
                        <td>Division 2</td>
                        <td>Division 3</td>
                    </tr>

                    <tr>
                        <td>Division 1</td>
                        <td>Division 2</td>
                        <td>Division 3</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
    <!-- /Section -->

    <br>
    <br>

    <!-- Section -->
    <footer class="main">
        <div class="section__inner">
            <nav class="actions">
                <a href="/example.html">Example</a>
                <a href="//github.com/ganda">GitHub</a>
            </nav>
        </div>
    </footer>
    <!-- /Section -->


</div>
<!-- /Page -->

<?php include 'inc/script.php'; ?>
</body>
</html>
