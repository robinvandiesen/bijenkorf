<!-- Menu -->
<header>
    <nav class="menu js-menu" role="navigation">
    	<div class="section__inner">
	        <input id="menu-toggle" type="checkbox" class="menu__toggle__target"></input>
	        <label for="menu-toggle" class="btn menu__toggle">Menu</label>

	        <a href="/" ><img class="menu__logo" src="assets/images/logo-bijenkorf.svg" alt="Bijenkorf"></a>

	        <ul class="menu__nav list">
	            <li><a href="/" class="menu__nav-link--active">Luxe merken</a></li>
	            <li><a href="#">Dames</a></li>
	            <li><a href="#">Heren</a></li>
	            <li><a href="#">Kinderen</a></li>
	            <li><a href="#">Wonen &amp; vrije tijd</a></li>
	            <li><a href="#">Cadeaus</a></li>
	            <li><a href="#">Outlet</a></li>
	            <li><a href="#">A-Z Merken</a></li>
	        </ul>
        </div>
    </nav>
</header>
<!-- /Menu -->