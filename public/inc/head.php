<?php include 'siteinfo.php'; ?>
<!doctype html>
<html lang="nl" class="no-js">
<head>

<meta charset="utf-8">
<title>[page title] — <?php echo $sitename;?></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, minimal-ui">

<meta name="apple-mobile-web-app-title" content="<?php echo $sitename;?>">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

<meta name="msapplication-TileImage" content="<?php echo $server;?>/apple-touch-icon-precomposed.png">
<meta name="msapplication-TileColor" content="#ffffff">


<!--- Twitter Card with Large Image -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="<?php echo $sitename;?>">
<meta name="twitter:creator" content="@">
<meta name="twitter:title" content="<?php echo $twitterTitle;?>">
<meta name="twitter:description" content="<?php echo $twitterDescription;?>">
<meta name="twitter:image" content="<?php echo $server;?><?php echo $twitterImage;?>">


<!--- Facebook -->
<meta property="og:site_name" content="<?php echo $sitename;?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo $server;?>">
<meta property="og:title" content="<?php echo $ogTitle;?>">
<meta property="og:description" content="<?php echo $ogDescription;?>">
<meta property="og:image" content="<?php echo $server;?> <?php echo $ogImage;?>">

<link href="<?php echo $server;?>/apple-touch-icon-precomposed.png" rel="image_src">
<link href="<?php echo $server;?>/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">

<link rel="icon" id="favicon" type="image/png" href="<?php echo $server;?>/assets/images/favicon.png" />
<link href="css/main.css" rel="stylesheet">

<script>
	// Remove class on init if js is enabled
	document.documentElement.classList.remove('no-js');
</script>
</head>