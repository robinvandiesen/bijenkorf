<?php include 'inc/head.php'; ?>

<body>
<a class="accessibility skip-to-main" href="#main">Skip to main content</a>

<!-- Page -->
<div class="page">

    <?php include 'inc/menu.php'; ?>

    <!-- Section -->
    <section class="section section-duo section-hero" id="main" tabindex="-1">
        <div class="section__inner">
            <h1>Make it <span>you</span></h1>
            <img class="section-duo__img--left" src="assets/images/woman.png" alt="Woman smiling">
            <img class="section-duo__img--right" src="assets/images/man.png" alt="Man wearing headphones">
        </div>
    </section>
    <!-- /Section -->

    <!-- Section -->
    <section class="section selection-row">
        <div class="section__inner" data-bg-title="Make it you">
            <h1>Kies je stijl</h1>
            <ul class="list">
                <li class="content-box"><a href="#"><img src="assets/images/1-casual.png" alt=""></a></li>
                <li class="content-box"><a href="#"><img src="assets/images/2-romantisch.png" alt=""></a></li>
                <li class="content-box"><a href="#"><img src="assets/images/3-biker.png" alt=""></a></li>
                <li class="content-box"><a href="#"><img src="assets/images/4-casualchic.png" alt=""></a></li>
                <li class="content-box"><a href="#"><img src="assets/images/5-casualfancy.png" alt=""></a></li>
            </ul>
        </div>
    </section>
    <!-- /Section -->

    <!-- Section -->
    <section class="section section-duo">
        <div class="section__inner">
            <h1>Trend 1 <span>Verlijdelijke lingerie</span></h1>
            <img class="section-duo__img--left" src="assets/images/1-lingerie.png" alt="">
            <img class="section-duo__img--right" src="assets/images/2-lingerie.png" alt="">
        </div>
    </section>
    <!-- /Section -->


</div>
<!-- /Page -->


<?php include 'inc/script.php'; ?>
</body>
</html>
