# Bijenkorf - Make it you

> Frontend for the Make it you project

## Build Setup

``` bash
# Install dependencies
npm install

# Run localhost
gulp
```

Gulp will also create a _dist folder where everthing is compiled to. The _dist is ment for distribution.

## Do not edit anything in the _dist folder!!! It will be overwritten when gulp is run.